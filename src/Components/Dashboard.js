import React, { Component } from 'react';
import Header from './Header';
import Cards from './Cards';
import Charts from './Charts';
import Tables from './Tables';

class Dashboard extends Component {
  render() {
    return (          
         <div style={{
            display: 'block',
            width:'100%',
            marginLeft: 10,
            height:'100%',
            backgroundColor: '#eeeeee',

          }}>
              <Header />
              <Cards />
              <Charts />
              <Tables />
       </div>
      );

    }
     
}

export default Dashboard;






     