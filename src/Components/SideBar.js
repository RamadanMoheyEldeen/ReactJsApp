import React, { Component } from 'react';
import {List, ListItem} from 'material-ui/List';
import ActionDashboard from 'material-ui/svg-icons/action/dashboard';
import ActionPermIdentity from 'material-ui/svg-icons/action/perm-identity';
import ContentContentPaste from 'material-ui/svg-icons/content/content-paste';
import AVLibraryBooks from 'material-ui/svg-icons/av/library-books';
import MapsPlace from 'material-ui/svg-icons/maps/place';
import EditorBubbleChart from 'material-ui/svg-icons/editor/bubble-chart';
import SocialNotifications from 'material-ui/svg-icons/social/notifications';
import { Link } from 'react-router-dom';


class SideBar extends Component {
  render() {
  		let imageUrl='https://vignette.wikia.nocookie.net/animal-jam-clans-1/images/7/71/Snow-mountains-background-1.jpg/revision/latest?cb=20160501132624';

    return (
      <div className="SideBar" style=
      {{ 
      	display:'flex' ,
      	width: '20.5%' ,
        backgroundColor: '#f8f9ee',
      	height: '100%',
      	backgroundImage: `url(${imageUrl})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      	opacity: 3,
    }}>


		   <List style={{
            padding: '7%'
		    }}>
    
      <h4 align= 'center'>CREATIVE TIM</h4>
      <hr style={{opacity:.5 }} />
      
      <Link style={{textDecoration: 'none'}}to={'/'} >
      <ListItem primaryText="Dashboard" hoverColor="purple" leftIcon={<ActionDashboard />} />
      </Link>
      <Link style={{textDecoration: 'none'}}to={'/profile'} >
      <ListItem primaryText="User Profile" hoverColor="purple" leftIcon={<ActionPermIdentity />} />
      </Link>

      <Link style={{textDecoration: 'none'}}to={'/list'} >
            <ListItem primaryText="Table List" hoverColor="purple" leftIcon={<ContentContentPaste />} />
      </Link>

      <Link style={{textDecoration: 'none'}}to={'/typo'} >
           <ListItem primaryText="Typography" hoverColor="purple" leftIcon={<AVLibraryBooks />} />
      </Link>

      <Link style={{textDecoration: 'none'}}to={'/icon'} >
      <ListItem primaryText="Icons" hoverColor="purple" leftIcon={<EditorBubbleChart />} />
      </Link>

     <Link style={{textDecoration: 'none'}}to={'/map'} >
      <ListItem primaryText="Maps" hoverColor="purple" leftIcon={<MapsPlace />} />
      </Link>


      <Link style={{textDecoration: 'none'}}to={'/notify'} >
         <ListItem primaryText="Notifications" hoverColor="purple" leftIcon={<SocialNotifications />} />
      </Link>


		    </List>
      </div>
    );
  }
}

export default SideBar;
