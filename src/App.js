import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import SideBar from './Components/SideBar';
import Dashboard from './Components/Dashboard';
import UserProfile from './Components/UserProfile';
import Icons from './Components/Icons';
import Maps from './Components/Maps';
import Notifications from './Components/Notifications';
import Typo from './Components/Typo';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';

injectTapEventPlugin();
class App extends Component {
  render() {
    return (
      <Router>
      <MuiThemeProvider>
      <div style={{
      	position: 'absolute',
      	width:'82.5%',
       	display: '-webkit-box' ,
      	height: '100%',
      	backgroundColor: '#eeeeee',

      }}>
      
         <SideBar/>
        	<Switch>
            <Route exact path={'/'} component={Dashboard} />
            <Route exact path={'/profile'} component={UserProfile} />
            <Route exact path={'/map'} component={Maps} />
            <Route exact path={'/icon'} component={Icons} />
            <Route exact path={'/list'} component={Icons} />
            <Route exact path={'/notify'} component={Notifications} />
            <Route exact path={'/typo'} component={Typo} />

          </Switch>
		</div>

      </MuiThemeProvider>
</Router>
    );
  }
}

export default App;
